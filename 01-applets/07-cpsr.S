	.globl	_start
_start:
	/*
	 * This program demonstrates arithmetic instructions and
	 * their effect on the Curret Program Status Register.
	 */

	/*
	 * Note, on the difference between `overflow' and `carry':
	 *
	 * - Overflow flags get set when the register cannot properly
	 *   represent the result as a signed value (you overflowed
	 *   into the sign bit).
	 *
	 * - Carry flags are set when the register cannot properly
	 *   represent the result as an unsigned value (no sign bit
	 *   required).
	 *
	 * Thanks to Seth, http://stackoverflow.com/a/6265950
	 */

	/*
	 * ARMv7-A CPSR format:
	 * Fields: |N|Z|C|V|Q|IT|J|RAZ | GE |  IT  |E|A|I|F|T|  M  |
	 * Length: |1|1|1|1|1| 2|1| 4  |  4 |   6  |1|1|1|1|1|  5  |
	*/

	mrs	r11, cpsr	@ original CPSR value
	ldr	r0, =0x00ffffff @ zero CPSR flag bits
	and	r11, r0
	msr	cpsr, r11	@ store final result

	/* -1 + -2: CPSR N (negative) and C (carry) set
	 * 0xffffffff + 0xfffffffe = 0x1fffffffd
	 * negative, no signed overflow, but with carry */
	mov	r0, #-1		@ or `mvn r1, #0'
	mov	r1, #-2		@ or `mvn r2, #1'
	adds	r2, r0, r1	@ `s' suffix: change the PSR!
	mrs	r10, cpsr	

	/* -3 + 2: CPSR N (negative) set, all others clear
	 * 0xfffffffd + 0x00000002 = 0xffffffff
	 * no negative, no signed overflow, no carry */
	mov	r0, #-3		@ or `mvn r1, #2'
	mov	r1, #2
	adds	r2, r0, r1	@ `s' suffix: change the PSR!
	mrs	r9, cpsr

	/* -3 + 7: CPSR C (carry) set, all others clear
	 * 0xfffffffd + 0x00000007 = 0x100000004
	 * no negative, no signed overflow, but with carry */
	mov	r0, #-3		@ or `mvn r1, #0'
	mov	r1, #7
	adds	r2, r0, r1	@ `s' suffix: change the PSR!
	mrs	r8, cpsr

	/* 1 + 2: All CPSR flags clear */
	mov	r0, #1
	mov	r1, #2
	adds	r2, r0, r1	@ `s' suffix: change the PSR!
	mrs	r7, cpsr

	/* CPSR C (carry) and V (signed overflow) set
	 * 0xA0000000 + 0xB0000000 = 0x150000000
	 * 0x50000000 trimmed result is positive, even though
	 * the two inputs are negative, thus the V bit is set!
	 * Total result does not fit 32-bits, thus C is set */
	mov	r0, #0xA0000000
	mov	r1, #0xB0000000
	adds	r2, r0, r1	@ total is 0x50000000, not negative
	mrs	r6, cpsr

	/* CPSR Z (zero) and C (carry) set
	 * 0xA0000000 + 0x60000000 = 0x100000000 */
	mov	r0, #0xA0000000
	mov	r1, #0x60000000
	adds	r2, r0, r1
	mrs	r5, cpsr

	/* x - x: CPSR Z (zero) and C (carry) set
	 *
	 * Why is carry set, even though we're doing `r0 - r0'?
	 * "Like older processors such as the MC68000 and its
	 * predecessors, the carry flag is _inverted_ after a
	 * substraction opteration, making the carry bit more
	 * like a borrow bit, primarily due to the way substr-
	 * action is implemented in hardware." */
	mov	r0, #0xff	@ ARM bit-rotation friendly value
	subs	r1, r0, r0	@ r0 = r0 - r0 = 0
	mrs	r4, cpsr

	/*
	 * 64-bit substraction
	 *
	 * 0xa000000010000000 - 0x1000000030000000 = 0x8fffffffe0000000L
	 *
	 * carry flag is _inverted_, and thus set to 0 (borrow performed)
	 * sbc = Rd - Rn - not(carry flag) = Rd - Rm - 1
	 *
	 * From ARM ARM: Subtract with Carry (register) subtracts an
	 * optionally-shifted register value and the value of NOT (Carry flag)
	 * from a register value, and writes the result to the destination
	 * register.
	 */
	ldr	r0, =0x10000000	@ lower 32-bits
	ldr	r1, =0xa0000000 @ upper 32-bits
	ldr	r2, =0x30000000 @ lower 32-bits
	ldr	r3, =0x10000000 @ upper 32-bits
	subs	r2, r0, r2
	sbc	r3, r1, r3	@ substract with carry!

	/* CPSR N (negative) and V (overflow) set
	 * 0x7fffffff + 0x7fffffff = 0xfffffffe
	 * (positive) + (positive) = (negative) -- overflow, no carry */
	ldr	r0, =0x7fffffff
	adds	r0, r0, r0
	mrs	r1, cpsr

	b	.

	/*
	 * After end of execution, expected result:
	 *
	 * r11 = 0x000001d3	@ CPSR, all clear
	 * r10 = 0xa00001d3	@ CPSR, with N and C set
	 * r9  = 0x800001d3	@ CPSR, all clear
	 * r8  = 0x200001d3	@ CPSR, with C set
	 * r7  = 0x000001d3	@ CPSR, all clear
	 * r6  = 0x300001d3	@ CPSR, with C and V set
	 * r5  = 0x600001d3	@ CPSR, with Z and C set
	 * r4  = 0x600001d3	@ CPSR, with Z and C set
	 * r3  = 0x8fffffff	@ upper 32-bit of 64-bit subtraction
	 * r2  = 0xe0000000	@ lower 32-bit of 64-bit subtraction
	 * r1  = 0x900001d3	@ CPSR, with N and V set
	 */
