	.globl _start
_start:
	/*
	 * This samples the use of ltorg directive, which controls
	 * the placement of the "literal pool" in the executable.
	 *
	 * The literal pool is used for storing constants that
	 * cannot fit using ARM immediate rotation scheme. For
	 * further details check:
	 *
	 * - ARM documentation for the LDR instruction
	 *
	 * - GNU Assembler (GAS) official docs for ARM directives
	 *   https://sourceware.org/binutils/docs/as/ARM-Directives.html
	 *
	 * - GAS generic list of directives
	 *   http://web.mit.edu/gnu/doc/html/as_7.html
	 */

	/*
	 * LDR uses PC-relative addressing for loading constants.
	 * But since we're introducing a huge 0xfff space in the
	 * middle of the .text section, PC-relative addressing can
	 * not work -- too big of an offset to be encoded.
	 */
	ldr	r0, =0xffffffff		@ cannot be encoded using ARM ..
	ldr	r1, =0x12345678		@ .. immediate rotation scheme
	b	continue

	/*
	 * As described above, enable LTORG directive below to avoid
	 * "Error: invalid literal constant: pool needs to be closer"
         */
	.ltorg

big_section:
	.fill	0xfff

	/* ARM instructions must be 4-byte aligned */
	.align	4
continue:
	b	.

	/*
	 * After end of execution, expected result:
	 *
	 * Nothing. This program will not compile if the `.ltorog'
	 * directive above is not added.
	 */
