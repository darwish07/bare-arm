#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

/*
 * arm-bit-rotation-scheme.c -- Given an immediate value
 * as first parameter, display if such value can be
 * represented using ARM immediate values rotation scheme
 *
 * Further explanation:
 *
 * - ARM Immediate Value Encoding, Alisdair McDiarmid
 *   http://alisdair.mcdiarmid.org/2014/01/12/arm-immediate-value-encoding.html
 *   http://www.webcitation.org/6ZUIJbKlz
 *
 * - ARM: Introduction to ARM: Immediate Values, David Thomas
 *   http://www.davespace.co.uk/arm/introduction-to-arm/immediates.html
 *   http://www.webcitation.org/6ZUJTu7R7
 *
 * - ARM ARM (ARM Architecture Reference Manual)
 */

/*
 * ARM MOV instruction only has 12-bit field for storing
 * immediate values. This is very limited. To go beyond
 * this constraint, ARM engineers invented a nice design:
 *
 * operand = 4-bit rotation + 8-bit immediate
 *
 * So final value = (8-bit immediate) ror (2 * rotation)
 *
 * Thus, we can encode the following values in the 12-bit
 * field, where `x' stand for any bit value 0 or 1:
 *
 *		(Binary form)		       (rotation)
 *              -------------                  ----------
 * 0000 0000 0000 0000 0000 0000 xxxx xxxx	   0
 * xx00 0000 0000 0000 0000 0000 00xx xxxx	   1
 * xxxx 0000 0000 0000 0000 0000 0000 xxxx	   2
 * xxxx xx00 0000 0000 0000 0000 0000 00xx	   3
 * xxxx xxxx 0000 0000 0000 0000 0000 0000	   4
 * 00xx xxxx xx00 0000 0000 0000 0000 0000         5
 * 0000 xxxx xxxx 0000 0000 0000 0000 0000         6
 * 0000 00xx xxxx xx00 0000 0000 0000 0000         7
 * 0000 0000 xxxx xxxx 0000 0000 0000 0000         8
 * 0000 0000 00xx xxxx xx00 0000 0000 0000         9
 * 0000 0000 0000 xxxx xxxx 0000 0000 0000	   10
 * 0000 0000 0000 00xx xxxx xx00 0000 0000	   11
 * 0000 0000 0000 0000 xxxx xxxx 0000 0000         12
 * 0000 0000 0000 0000 00xx xxxx xx00 0000         13
 * 0000 0000 0000 0000 0000 xxxx xxxx 0000         14
 * 0000 0000 0000 0000 0000 00xx xxxx xx00         15
 *
 * Thus, we can store the following values as:
 *
 *     (Value)	     (Immediate)	(rotation)
 *   0x000000ff		0xff		    0
 *   0xc000003f		0xff		    1
 *   0xf000000f		0xff		    2
 *   0xfc000003		0xff                3
 *   0xff000000		0xff		    4
 *   0x3fc00000		0xff		    5
 *   0x0ff00000		0xff		    6
 *   0x03fc0000		0xff		    7
 *   0x00ff0000		0xff		    8
 *   0x003fc000		0xff		    9
 *   0x000ff000		0xff		    10
 *   0x0003fc00		0xff		    11
 *   0x0000ff00		0xff		    12
 *   0x00003fc0		0xff		    13
 *   0x00000ff0		0xff		    14
 *   0x000003fc		0xff		    15
 */

int main(int argc, char **argv)
{
	uint32_t encode;
	int rotate;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <immediate-value>\n", argv[0]);
		exit(-1);
	}

	encode = strtoul(argv[1], NULL, 0);

	// Brute-force search
	for (rotate = 0; rotate < 32; rotate += 2) {
		printf("rotate = %4d, encode = 0x%8x\n", rotate, encode);

		// Did we find a pure 8-bit immediate value?
		if (!(encode & ~0xffU))
			printf("** Result = 0x%X%02X\n", rotate/2, encode);

		encode = (encode << 2) | (encode >> 30);
	}

	return 0;
}
