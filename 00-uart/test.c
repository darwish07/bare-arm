#ifdef VEXPRESS_A9
#define UART_ADDR	0x10009000
#else
#define UART_ADDR	0x101f1000
#endif

static volatile unsigned int * const UART0DR = (unsigned int *)UART_ADDR;

static void print_uart0(const char *s)
{
	while(*s != '\0') {
		*UART0DR = (unsigned int)(*s);
		s++;
	}
}

void c_entry()
{
	print_uart0("Hello world!\n");
}
