#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

/*
 * This program aims at validating the hamming ARM assembly
 * program located at 01-applets/09-hamming.S
 */

/*
 * Given a byte that needs to transmitted in the form:
 *	d7 d6 d5 d4  d3 d2 d1 d0
 *
 * We need to generate a hamming code of the form:
 *	d7 d6 d5 d4  c3 d3 d2 d1  c2 d0 c1 c0
 *
 * where:
 *	c0 = even_parity(d0, d1, d3, d4, d6)
 *	c1 = even_parity(d0, d2, d3, d5, d6)
 *	c2 = even_parity(d1, d2, d3, d7)
 *	c3 = even_parity(d4, d5, d6, d7)
 *
 * and:
 *	even_parity(x0,..,xn) equals one if the total number
 *	of high bits in x0,..,xn is odd, where the parity
 *	bits complements that number of high bits into even.
 *	The result of even_parity(x0,..,xn) is 0 if the total
 *	number of positive bits is already even.
 */

unsigned int hamming(unsigned int input)
{
	unsigned int c0, c1, c2, c3;
	unsigned int output;

	/*
	 * c0 = d0 xor d1 xor d3 xor d4 xor d6
	 * c1 = d0 xor d2 xor d3 xor d5 xor d6
	 * c2 = d1 xor d2 xor d3 xor d7
	 * c3 = d4 xor d5 xor d6 xor d7
	 */
	c0 = (input ^ (input >> 1) ^ (input >> 3) ^ (input >> 4) ^ (input >> 6)) & 1;
	c1 = (input ^ (input >> 2) ^ (input >> 3) ^ (input >> 5) ^ (input >> 6)) & 1;
	c2 = ((input >> 1) ^ (input >> 2) ^ (input >> 3) ^ (input >> 7)) & 1;
	c3 = ((input >> 4) ^ (input >> 5) ^ (input >> 6) ^ (input >> 7)) & 1;

	/*
	 * output = d7 d6 d5 d4 c3 d3 d2 d1 c2 d0 c1 c0
	 */
	output  = 0;
	output |= c0;				// c0 (bit #0)
	output |= c1 << 1;			// c1 (bit #1)
	output |= (input & 1) << 2;		// d0 (bit #2)
	output |= c2 << 3;			// c2 (bit #3)
	output |= ((input >> 1) & 0x7) << 4;	// d1, d2, d3 (bit #4, #5, #6)
	output |= c3 << 7;			// c3 (bit #7)
	output |= (input >> 4) << 8;		// d4, d5, d6, d7 (bit #8, ...)

	return output;
}

int main(int argc, char **argv)
{
	unsigned int input;

  	if (argc != 2) {
		fprintf(stderr, "Usage: %s <8-bit value>\n", argv[0]);
		return -1;
	}

	input = strtoul(argv[1], NULL, 0);
	if (input > 0xff) {
		fprintf(stderr, "Error: got larger than 8-bit value 0x%x\n",
			input);
		return -2;
	}

	printf("Hamming code for 0x%x = 0x%x\n", input, hamming(input));

	return 0;
}
